const test = require('tape')
const { initialTransformationSimpleSet, simpleSetChange } = require('..')

test('transformSet', t => {
  const apiSets = [
    {},
    { add: null },
    { add: undefined },
    { dog: [] },
    { add: [] },
    { add: [], remove: [] }, // NOTE: remove should be ignored
    { // add single item
      add: ['cherese']
    },
    {
      add: ['cheche', 'reese']
    },
    { // add + remove the same item
      add: ['reese'],
      remove: ['reese'] // should be ignored
    }
  ]

  const expectedTransform = [
    {},
    {},
    {},
    {},
    {}, // sets that have { add: [] } return empty T
    {},
    // SIMPLE SETS - altNames
    { cherese: 1 },
    { cheche: 1, reese: 1 },
    { reese: 1 }
  ]

  apiSets.forEach((input, i) => {
    t.deepEqual(
      initialTransformationSimpleSet(input),
      expectedTransform[i],
      `transformSet : ${JSON.stringify(input)}`
    )
  })

  t.end()
})

test('simpleSetChange', t => {
  var initialStates = [
    {}, // 1: add to empty state
    { cherese: 1 }, // 2: add to a state with an existing item
    { cherese: 1 },
    {
      mixmix: -2,
      mixmixjellyfish: -1,
      'j a irving': 1
    },
    {
      cherese: -1
    }
  ]

  var updates = [
    {
      add: ['cherese']
    },
    {
      add: ['cheche']
    },
    {
      remove: ['cherese']
    },
    {
      add: ['mixmixjellyfish', 'jai'],
      remove: ['j a irving']
    },
    {
      add: ['cherese']
    }
  ]

  var expectedStates = [
    {
      cherese: 1
    },
    {
      cheche: 1
    },
    { cherese: -1 },
    {
      mixmixjellyfish: 2,
      jai: 1,
      'j a irving': -1
    },
    {
      cherese: 2
    }
  ]

  initialStates.forEach((initialState, i) => {
    t.deepEqual(
      simpleSetChange(
        initialState,
        updates[i]
      ),
      expectedStates[i],
      JSON.stringify(updates[i])
    )
  })

  t.end()
})
