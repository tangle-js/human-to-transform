const get = require('lodash.get')
const setWith = require('lodash.setwith')
const isEqual = require('lodash.isequal')

function initialTransformationSimpleSet (set) {
  if (typeof set !== 'object') return {}

  var { add } = set
  if (!add) return {}

  var output = {}

  add.forEach(k => {
    output[k] = 1
  })
  // NOTE ignore remove commands (doesn't make sense to remove when creating)

  return output
}

function initialTransformationComplexSet (set) {
  if (typeof set !== 'object') return {}

  var { add } = set
  if (!add) return {}

  var output = {}

  add.forEach(({ id, seq }) => {
    output[id] = { [seq]: 1 }
  })
  // NOTE ignore remove commands (doesn't make sense to remove when creating)

  return output
}

/*
  gets the new state from the current state + a change for a simple set
*/
function simpleSetChange (setState, userChange) {
  if (!userChange) return
  if (typeof userChange === 'object' && !Object.keys(userChange).length) return

  const { add = [], remove = [] } = userChange

  if (add && !Array.isArray(add)) throw new Error('simpleSet.add must be an Array of Strings')
  if (remove && !Array.isArray(remove)) throw new Error('simpleSet.remove must be an Array of Strings')

  const change = {}

  add.forEach(key => {
    if (setState[key] !== undefined) {
      if (setState[key] < 1) {
        change[key] = -1 * setState[key] + 1
      }
    } else {
      change[key] = 1
    }
  })

  remove.forEach(key => {
    if (setState[key] !== undefined) {
      if (setState[key] >= 1) {
        change[key] = -1 * setState[key]
      }
    }
  })

  return change
}

/*
  gets the new state from the current state + a change
*/
function complexSetChange (setState, userChange) {
  if (!userChange) return
  if (typeof userChange === 'object' && !Object.keys(userChange).length) return

  const { add = [], remove = [] } = userChange

  if (add && !Array.isArray(add)) throw new Error('a complex set must be an Array of Objects')
  if (remove && !Array.isArray(remove)) throw new Error('a complex set must be an Array of Objects')

  if (add.length === 0 && remove.length === 0) return {}

  const addErrors = add.some(d => !d.id || !d.seq)
  const rmErrors = remove.some(d => !d.id || !d.seq)

  if (addErrors || rmErrors) throw new Error(`Found errors in the given add or remove for a complexSetChange. SetState: ${JSON.stringify(setState)}, UserChange: ${JSON.stringify(userChange)}`)

  // check to see if there exists an item that is in both arrays
  const intersection = add.some(d => {
    return remove.some(rm => {
      if (isEqual(rm, d)) return true
      return rm.id === d.id
    })
  })

  if (intersection) throw new Error(`Found errors. There is same id in the add + remove fields for a complex set. SetState: ${JSON.stringify(setState)}, UserChange: ${JSON.stringify(userChange)}`)

  const change = {}

  add.forEach(({ id, seq }) => {
    const current = get(setState, [id, seq])
    if (current && current < 1) {
      setWith(change, [id, seq], -1 * current + 1, Object)
    } else {
      setWith(change, [id, seq], 1, Object)
    }
  })

  remove.forEach(({ id, seq }) => {
    const current = get(setState, [id][seq])
    if (current && current >= 1) {
      setWith(change, [id, seq], -1 * current, Object)
    } else {
      setWith(change, [id, seq], -1, Object)
    }
  })

  return change
}

module.exports = {
  initialTransformationSimpleSet,
  initialTransformationComplexSet,
  simpleSetChange,
  complexSetChange
}
