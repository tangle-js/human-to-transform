const test = require('tape')
const { initialTransformationComplexSet, complexSetChange } = require('..')

test('transformComplexSet', async t => {
  const apiSets = [
    {},
    { add: null },
    { add: undefined },
    { dog: [] },
    { add: [] },
    { add: [], remove: [] }, // NOTE: remove should be ignored
    // COMPLEX SETS
    { // add single item
      add: [{ id: '@cherese', seq: 200 }]
    },
    { // add + remove the same item
      add: [{ id: '@cherese', seq: 200 }],
      remove: [{ id: '@cherese', seq: 200 }] // should be ignored
    },
    { // add multiple
      add: [
        { id: '@ben', seq: 400 },
        { id: '@mix', seq: 500 }
      ]
    },
    { // add multiple reversed order
      add: [
        { id: '@mix', seq: 500 },
        { id: '@ben', seq: 400 }
      ]
    }
  ]

  const expectedTransform = [
    {},
    {},
    {},
    {},
    {}, // sets that have { add: [] } return empty T
    {},
    // COMPLEX SETS - authors
    { '@cherese': { 200: 1 } },
    { '@cherese': { 200: 1 } }, // remove operation was ignored
    {
      '@ben': { 400: 1 },
      '@mix': { 500: 1 }
    },
    {
      '@ben': { 400: 1 },
      '@mix': { 500: 1 }
    },
    {}
  ]

  apiSets.forEach((input, i) => {
    t.deepEqual(
      initialTransformationComplexSet(input),
      expectedTransform[i],
      `transformSet : ${JSON.stringify(input)}`
    )
  })

  t.end()
})

test('complexSetChange', async t => {
  t.plan(13)
  var initialStates = [
    {}, // 1: empty initial state, empty update
    {}, // 2: add item to empty initial state
    { '@cherese': { 200: 1 } }, // 3: add item to non-empty initial state
    { '@cherese': { 200: 1 } }, // 4: remove item from state
    { '@cherese': { 200: 1 } }, // 5: attempt to add the same seq
    { '@cherese': { 200: 1, 500: -1 } }, // 6: attempt to remove the same seq
    { '@cherese': { 200: -1 } }, // 7: re-add a removed state
    {},
    {},
    {},
    {},
    {},
    {}
  ]

  var updates = [
    {}, // 1
    { // 2
      add: [{ id: '@cherese', seq: 100 }]
    },
    { // 3
      add: [{ id: '@mixmix', seq: 200 }]
    },
    { // 4
      remove: [{ id: '@cherese', seq: 400 }]
    },
    { // 5
      add: [{ id: '@cherese', seq: 200 }]
    },
    { // 6
      remove: [{ id: '@cherese', seq: 500 }]
    },
    { // 7
      add: [{ id: '@cherese', seq: 200 }]
    },
    { // 8
      add: [{ id: '@cherese', seq: 200 }],
      remove: [{ id: '@cherese', seq: 200 }]
    },
    {
      add: [],
      remove: []
    },
    {
      add: [{}],
      remove: [{}]
    },
    {
      add: [{ name: 'Cherese' }],
      remove: [{ name: 'Cherese' }]
    },
    {
      add: [{ someField: 'hello' }],
      remove: []
    },
    {
      add: [],
      remove: [{ someField: 'hello' }]
    }
  ]

  const throws = true

  var expectedStates = [
    undefined, // 1
    { '@cherese': { 100: 1 } }, // 2
    { '@mixmix': { 200: 1 } }, // 3
    { '@cherese': { 400: -1 } }, // 4
    { '@cherese': { 200: 1 } }, // 5
    { '@cherese': { 500: -1 } }, // 6
    { '@cherese': { 200: 2 } }, // 7
    { throws },
    {},
    { throws },
    { throws },
    { throws },
    { throws }
  ]

  initialStates.forEach(async (initialState, i) => {
    if (expectedStates[i] && expectedStates[i].throws) {
      await t.throws(
        () => (
          complexSetChange(initialState, updates[i], expectedStates[i])
        ),
        /errors/,
        'throws error on same id used in add + remove'
      )
    } else {
      t.deepEqual(
        complexSetChange(
          initialState,
          updates[i]
        ),
        expectedStates[i],
        JSON.stringify(updates[i])
      )
    }
  })
})
